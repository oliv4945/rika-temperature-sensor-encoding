#ifndef __RIKA_PAYLOAD
#define __RIKA_PAYLOAD

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdint.h>

/**************       CONSTANTS       ******************/
#define RIKA_PAYLOAD_SIZE 7
#define RIKA_HEADER 0x11
#define RIKA_ADDRESS 0x00

    /*!
     * @brief Add the CRC and apply whitening on a buffer
     *
     * @remark
     *      This is only requires when the radio encoding is not compatible
     *      with the one used by Rika (TI - CC1101). Ex: Semtech LR1110
     *
     * @param [inout] buffer Buffer to process. MUST be more than 6 bytes!
     *
     * @returns Encoded temperature
     */
    void add_rika_crc_whitening(uint8_t *buffer);

    /*!
     * @brief Encode a buffer as a Rika wireless temperature sensor
     *
     * @param [inout] buffer Buffer to store the payload. MUST be more than 4 bytes!
     * @param [in] temperature Temperature to be sent, in °C
     * @param [in] battery_level Battery level, in V
     */
    void create_rika_payload(uint8_t *buffer, const float temperature, const float battery_level);

    /*!
     * @brief Encode a battery value in Rika format
     *
     * @param [in] battery Battery value to be encoded, in volt
     *
     * @returns Encoded battery value
     */
    uint8_t encode_rika_battery(float battery);

    /*!
     * @brief Encode a temperature value in Rika format
     *
     * @param [in] temperature Temperature value to be encoded, in °C
     *
     * @returns Encoded temperature value
     */
    uint16_t encode_rika_temperature(float temperature);

#ifdef __cplusplus
}
#endif

#endif // ifndef __RIKA_PAYLOAD