#include <stdio.h>
#include "../rika_payload.h"

int main(void)
{
  uint8_t buffer[RIKA_PAYLOAD_SIZE];
  float battery_level = 2.5;
  float temperature = 18.9;
  printf("Parameters:\n  - Temperature: %.2f°C\n  - Remaining battery: %.2f V\n", temperature, battery_level);

  // Fill the buffer with a Rika compatible frame
  create_rika_payload(buffer, temperature, battery_level);
  printf("Rika encoded payload:\n  - 0x");
  for (long unsigned int i = 0; i < 5; i++)
  {
    printf("%.2X ", buffer[i]);
  }
  printf("\n");
  // Add software CRC and whitening for radio using different parameters than CC1101
  add_rika_crc_whitening(buffer);

  printf("Encoded Rika payload + CRC:\n  - 0x");
  for (long unsigned int i = 0; i < sizeof(buffer); i++)
  {
    printf("%.2X ", buffer[i]);
  }
  printf("\n");
}
