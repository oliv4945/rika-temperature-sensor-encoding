C library to create Rika compatible payloads
---

# Purpose

This library has been created to impersonate Rika pellet stove wireless sensors. The aim of this project is to be able to remote control my stove.  
Two main functions are:

* `create_rika_payload(uint8_t *buffer, const float temperature, const float battery_level)`: Create the buffer with data encoded for Rika stove.
* `add_rika_crc_whitening(uint8_t *buffer)`: Add software CRC and whitening for radio chips with different whitening or CRC parameters than TI CC1101.

More info about this project and the reverse engineering on this [blog post](https://notes.iopush.net/blog/2022/12-rika-wireless-temperature-1/).

# Usage

1. Include `rika_payload.h`
2. Call `create_rika_payload()`
3. Optional: call `add_rika_crc_whitening()`

The provided example can be compiled with `make`, then executed with `./rika_payload`.

# Compatibility

Currently tested on:

* [Rika Filo](https://www.rika.eu/filo)
* [Rika Sumo](https://www.rika.eu/sumo)

Please let me know if you try it with other stoves.