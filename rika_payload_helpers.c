#include <stdlib.h>
#include <stdint.h>
#include "rika_payload_helpers.h"

unsigned char byte_swap_8(unsigned char b)
{
  b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
  b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
  b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
  return b;
}

uint16_t compute_crc_16(const uint8_t buffer[], const unsigned int length)
{
  uint16_t crc_16 = 0xFFFF;

  for (unsigned int i = 0; i < length; i++)
  {
    uint16_t tableValue = crc16table[((crc_16 >> 8) ^ *(uint8_t *)buffer++) & 0x00FF];
    crc_16 = (crc_16 << 8) ^ tableValue;
  }

  return crc_16;
}

int whitening_pn9(uint8_t *buf, uint8_t len)
{
  if (len > sizeof(pn9_table))
    return -1;

  for (int i = 0; i < len; ++i)
  {
    buf[i] ^= pn9_table[i];
  }

  return 0;
}
