#include <stdlib.h>
#include <stdint.h>
#include "rika_payload.h"
#include "rika_payload_helpers.h"

void add_rika_crc_whitening(uint8_t *buffer)
{
  uint16_t crc_16;

  // Add CRC
  crc_16 = compute_crc_16(buffer, 5);
  buffer[5] = (crc_16 >> 8) & 0xFF;
  buffer[6] = crc_16 & 0xFF;

  // Apply whitening
  whitening_pn9(buffer, RIKA_PAYLOAD_SIZE);
}

void create_rika_payload(uint8_t *buffer, const float temperature, const float battery_level)
{
  uint16_t temperature_converted;

  // Add Rika header and address
  buffer[0] = RIKA_HEADER;
  buffer[1] = RIKA_ADDRESS;

  // Add temperature
  temperature_converted = encode_rika_temperature(temperature);
  buffer[2] = (temperature_converted >> 8) & 0xFF;
  buffer[3] = temperature_converted & 0xFF;

  // Add battery level
  buffer[4] = encode_rika_battery(battery_level);
}

uint8_t encode_rika_battery(float battery)
{
  return (uint8_t)(battery * 100.02 - 87.556);
}

uint16_t encode_rika_temperature(float temperature)
{
  return (uint16_t)((temperature + 109.91) / 0.0068);
}
